//
//  Admin_ContentSceene.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import UIKit

class Admin_ContentSceene: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var collection: UICollectionView!
    
    private let ContentDTO = HomeDTO.init()
    
    @IBAction func save(_ sender: UIBarButtonItem) {
        AdminLayer.SAVE_CONTENT(content: self.ContentDTO) {
            print("complated")
        }
    }
    fileprivate var Images = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    fileprivate func config() {
        collectionConfig()
        textConfig()
    }
    private func collectionConfig() {
        collection.delegate = self
        collection.dataSource = self
    }
    private func textConfig() {
        titleTextField.delegate = self
        contentTextView.delegate = self
    }
}
extension Admin_ContentSceene : UICollectionViewDelegate , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Admin_ContentCell", for: indexPath) as? Admin_ContentCell else { return UICollectionViewCell() }
        cell.ContentImage.image = UIImage()
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    private func imageAlert() {
        DynamicAlert.shared._alert(AlertActionTypes: [.TakePhoto,.ChooseFromLibrary,.Cancel],
                                   controller: self,
                                   PreffredStyle: UIAlertController.Style.alert,
                                   component_iPAD: nil,
                                   title: "Choose",
                                   message: nil)
        { (selected) in
            guard let Selected = selected else { return }
            switch Selected {
            case .TakePhoto :
                self.getImage(fromSourceType: .camera)
            case .ChooseFromLibrary:
                self.getImage(fromSourceType: .photoLibrary)
            default :
                break
            }
        }
    }
    func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        
        //Check is source type available
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }

}
extension Admin_ContentSceene : UITextFieldDelegate, UITextViewDelegate {
    func textViewDidEndEditing(_ textView: UITextView) {
        switch textView {
        case contentTextView:
            ContentDTO.description = textView.text
        default:
            break
        }
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        switch textField {
        case titleTextField:
            ContentDTO.title = text
        default:
            break
        }
    }
}
extension Admin_ContentSceene : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage, let base64 = image.toBase64() {
            self.Images.append(base64)
        }
    }
}
