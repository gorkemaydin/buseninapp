//
//  BlogTravelList.swift
//  BuseninApp
//
//  Created by GÖRKEM AYDIN on 21.05.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import UIKit


class BlogTravelList : UITableViewController {
    fileprivate var List = [HomeDTO]()
    
    private func getList() {
        BlogListLayer.GET_BLOG_LIST { (_list) in
            guard let list = _list else { return }
            self.List = list
            self.tableView.reloadData()
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        getList()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BlogTravelListCell") as? BlogTravelListCell else { return UITableViewCell() }
        cell.travelTitle.text = List[indexPath.row].title
        cell.travelDescription.text = List[indexPath.row].description
        return cell
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
