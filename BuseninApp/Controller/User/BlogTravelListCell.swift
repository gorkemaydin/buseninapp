//
//  BlogTravelListCell.swift
//  BuseninApp
//
//  Created by GÖRKEM AYDIN on 21.05.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import UIKit

class BlogTravelListCell : UITableViewCell {
    
    @IBOutlet weak var travelImage: UIImageView!
    @IBOutlet weak var travelTitle: UILabel!
    @IBOutlet weak var travelDescription: UILabel!
    
    override func awakeFromNib() {
        //UIDesign Here
    }
}
