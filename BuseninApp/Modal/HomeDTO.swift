//
//  HomeDTO.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

open class HomeDTO : Codable , JSONDecodable{
    
    public var title : String?
    public var description : String?
    public var image : String?
    
    public init() {
        title = nil
        description = nil
        image = nil
    }
    public func encode(to encoder: Swift.Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(title, forKey: .title)
        try container.encode(description, forKey: .description)
        try container.encode(image, forKey: .image)
    }
    public required init?(json: JSON) {
        if let Title : String = "title" <~~ json  { self.title = Title }
        if let Description : String = "description" <~~ json { self.description = Description }
        if let Image : String = "image" <~~ json { self.image = Image }
    }
}
