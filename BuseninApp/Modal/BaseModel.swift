//
//  BaseModel.swift
//  BuseninApp
//
//  Created by GÖRKEM AYDIN on 26.05.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class BaseModal< T : JSONDecodable> : JSONDecodable {
    
    public var message : String?
    public var isError : Bool!
    public var data : [T]?
    
    public required init?(json: JSON) {
       
        if let Message : String = "message" <~~ json { self.message = Message }
        if let IsError : Bool = "isError" <~~ json { self.isError = IsError }
        if let Data : [T] = "data" <~~ json { self.data = Data }
     }
}


