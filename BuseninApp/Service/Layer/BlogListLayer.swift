//
//  BlogListLayer.swift
//  BuseninApp
//
//  Created by GÖRKEM AYDIN on 21.05.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Foundation
import Gloss

class BlogListLayer {
    
    static func GET_BLOG_LIST(complated : @escaping ([HomeDTO]?) -> () )
    {
        HTTPRequest.shared.Service(method: "GET", path: ServicePaths.GetBlogList, requestObj: AnyNil)
        {
            response in
            guard let JSONData = response as? JSON, let Base = BaseModal<HomeDTO>(json: JSONData) else { return }
//            if Base.isError {
//                complated(nil)
//                return
//            }
            complated(Base.data)
        }
    }
}
