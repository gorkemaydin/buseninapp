//
//  AdminLayer.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Gloss

open class AdminLayer  {
    
    static func SAVE_CONTENT<T : Codable>(content : T, complated : @escaping () -> () )
    {
        HTTPRequest.shared.Service(method: "POST", path: ServicePaths.SaveContent , requestObj: content)
        {
            response in
            print(response)
        }
        
    }
}
