//
//  HTTPRequest.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Foundation
import Alamofire

public let AnyNil = AnyCodable(nilLiteral: ())

open class HTTPRequest {
    
    static let shared = HTTPRequest()
    fileprivate let afManager = Alamofire.SessionManager()
   
    public func Service<T : Codable> (method:String,
                            path:String,
                            enablePop : Bool = true,
                            additionalPath : String? = nil,
                            parameters:[String:String]? = nil,
                            requestObj: T? ,
                            complated : @escaping (_ some : Any) -> Void = { _ in}) {
        
        
        var _url = URLComponents(string: StaticConstants.url + path)
        
        if let AddPath = additionalPath {
            _url = URLComponents(string: StaticConstants.url + path  + AddPath)
        }

        //let trace = Performance.startTrace(name: "isbasi")
        if parameters != nil{
            var queryItems = [URLQueryItem]()
            for parameter in parameters!{
                let queryItem = URLQueryItem(name: parameter.key, value: parameter.value)
                queryItems.append(queryItem)
            }
            _url?.queryItems = queryItems
        }
        var request = URLRequest(url: (_url?.url)!)
        
        request.httpMethod = method
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        if !(requestObj as? AnyCodable == AnyNil){
            do {
                let jsonEncoder = JSONEncoder()
                request.httpBody = try jsonEncoder.encode(requestObj)
            } catch let jsonErr {
                print("failed to decode, \(jsonErr)")
            }
        }
        afManager.request(request).responseJSON {
            response in
            
            if let code = response.response?.statusCode {
                if code == 200, let _JSON = response.result.value  {
                        complated(_JSON)
                }else {
                    print("getting JSON error")
                }
            }
        }
    }
}

//setCertificate()
//AsyncTask.group.enter()
//defer { AsyncTask.group.leave() }
