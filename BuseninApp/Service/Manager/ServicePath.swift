//
//  ServicePath.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Foundation

open class ServicePaths  {
    static let SaveContent = "buseninlist.php"
    static let GetBlogList = "getbuseninlist.php"
}
