//
//  Alert_Types.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import Foundation

import Foundation

class Alert_Types {
    enum _Type {
        case TakePhoto
        case ChooseFromLibrary
        case Cancel
    }
}
