//
//  DynamicAlert.swift
//  BuseninApp
//
//  Created by Görkem Aydın on 11.04.2019.
//  Copyright © 2019 Görkem Aydın. All rights reserved.
//

import UIKit

class DynamicAlert {
    
    static let shared = DynamicAlert()
    
    public func _alert( AlertActionTypes : [Alert_Types._Type],
                        controller : UIViewController ,
                        PreffredStyle : UIAlertController.Style = .alert ,
                        component_iPAD : Any? = nil,
                        title : String? ,
                        message : String? ,
                        Complated : @escaping (Alert_Types._Type?) -> ())
    {
        
        let Alert = UIAlertController(title: title, message: message, preferredStyle: PreffredStyle)
        var style : UIAlertAction.Style = UIAlertAction.Style.default
        
        if let popoverController = Alert.popoverPresentationController , let barbutton = component_iPAD as? UIBarButtonItem{
            popoverController.barButtonItem = barbutton
        }
        
        for AlertActionType in AlertActionTypes {
            
            if AlertActionType == .Cancel{
                style = UIAlertAction.Style.destructive
            }else {
                style = UIAlertAction.Style.default
            }
            
            Alert.addAction(UIAlertAction(title: AlertActionsTitle(type: AlertActionType), style: style, handler: { (Alert) in
                Complated(AlertActionType)
            }))
        }
        controller.present(Alert, animated: true) {
            Complated(nil)
        }
    }
    public func _CloseControllerAlert(controller : UIViewController , complated : @escaping  () -> () ) {
        _alert(AlertActionTypes: [.Cancel],
               controller: controller,
               PreffredStyle: .alert,
               component_iPAD: nil,
               title: NSLocalizedString("ExitPopUp", comment: "ExitPopUp" ),
               message: nil)
        { (type) in
            switch type {
            case .Cancel?:
                controller.dismiss(animated: true, completion: nil)
                complated()
            default:
                break
            }
        }
    }
    
    private func AlertActionsTitle(type : Alert_Types._Type)-> String{
        switch type {

        case .ChooseFromLibrary:
            return NSLocalizedString("photoLibrary", comment: "")
        case .TakePhoto:
            return NSLocalizedString("takePhoto", comment: "")
        case .Cancel:
            return NSLocalizedString("cancel", comment: "")
        }
    }
}
